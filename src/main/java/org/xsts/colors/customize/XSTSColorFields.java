/*
 * Group : XSTS
 * Project : Colors
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.colors.customize;

public class XSTSColorFields {
    public static final String ROUTE_SHORT_NAME = "route_short_name";
    public static final String RED_CHANNEL = "red_channel";
    public static final String GREEN_CHANNEL = "green_channel";
    public static final String BLUE_CHANNEL = "blue_channel";
    public static final String COLOR_HEX_VALUE= "color_hex_value";

    public static final String SHORT_NAME = ROUTE_SHORT_NAME;
    public static final String RED = RED_CHANNEL;
    public static final String GREEN = GREEN_CHANNEL;
    public static final String BLUE = BLUE_CHANNEL;
    public static final String HEX = COLOR_HEX_VALUE;
}
