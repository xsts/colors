/*
 * Group : XSTS
 * Project : Colors
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.colors.customize;

import org.xsts.core.data.collections.KeyValueList;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class XSTSColorList implements KeyValueList {
    private Map<String, XSTSColor> colors;

    public XSTSColorList(){
        colors = new HashMap<>();
    }

    @Override
    public void add(String id, Object object) {
        add(id, (XSTSColor) object);
    }

    public void add(String shortName, XSTSColor XSTSColor){
        colors.put(shortName, XSTSColor);
    }
    public XSTSColor get(String shortName) {
        return colors.get(shortName);
    }
    public boolean isEmpty() { return colors.size() == 0;}
    public XSTSColor first() {
        return colors.get(colors.keySet().toArray()[0]);
    }
    public Set<String> keySet() { return colors.keySet(); }
}
